function invert(obj){
    var inv_obj = {};
    for(var key in obj){
      inv_obj[obj[key]] = key;
    }
    return inv_obj;
  }
module.exports = invert;