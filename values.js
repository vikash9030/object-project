function values(obj){
    var newArray = [];
    for(var key in obj){
        if(obj.hasOwnProperty(key)){
            newArray.push(obj[key]);
        }

    }
    
    return newArray;
}

module.exports = values;