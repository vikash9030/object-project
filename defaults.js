function defaults(obj, defaultProps){
     
    if(obj === undefined){
        return defaultProps;
    }else{
        return obj;
    }   
}

module.exports = defaults;

