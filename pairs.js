//const values = require("../values.js");

const keys = require("./keys.js");
const values = require("./values.js");


function pairs(obj){
    var ans = [];
    let arr1 = keys(obj);
    let arr2 = values(obj);
    // console.log(arr1);
    // console.log(arr2);
    for(let index =0 ;index<arr1.length;index++){
        var newArray =[];
        newArray.push(arr1[index]);
        newArray.push(arr2[index]);
        ans.push(newArray);

    }
return ans;
  

}
module.exports =pairs;
// const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

// console.log(pairs(testObject));