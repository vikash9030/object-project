const keys = require("./keys.js");
function mapObject(obj, cb){
    const arr1 = keys(obj);
    for(let index = 0; index < arr1.length; index++){
        obj[arr1[index]] = cb(obj,arr1[index]);;
    }
    return obj;
}
module.exports = mapObject;